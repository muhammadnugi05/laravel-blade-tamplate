@extends('layouts.master')
@section('header')
<h1>Edit Cast {{$cast->id}}</h1>
@endsection
@section('content')
<h1>Form</h1>
<form role="form" method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method('put')
    <div class="box-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value={{$cast->nama}} placeholder="Nama Cast">
        @error('nama')
          <div class="alert alert-danger">
            {{$message}}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Umur</label>
        <input type="text" class="form-control" name="umur" id="umur" value={{$cast->umur}} placeholder="Umur Cast">
        @error('umur')
        <div class="alert alert-danger">
          {{$message}}
        </div>
      @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <input type="text" class="form-control" name="bio" id="bio" value={{$cast->bio}} placeholder="Bio Cast">
        @error('bio')
        <div class="alert alert-danger">
          {{$message}}
        </div>
      @enderror
      </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
      <button type="submit" class="btn btn-primary">Create</button>
    </div>
  </form>
@endsection