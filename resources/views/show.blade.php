@extends('layouts.master')
@section('header')
<h1>Show cast {{$cast->id}}</h1>
@endsection
@section('content')
<div class="card" style="width: 18rem;">
    <div class="card-body">
      <h4 class="card-title">{{$cast->nama}}</h4><br>
      <h6 class="card-sub mb-2">Umur: {{$cast->umur}}</h6>
      <p class="card-text">{{$cast->bio}}</p>
      <a href="/cast" class="btn btn-primary">Kembali</a>
    </div>
  </div>
@endsection