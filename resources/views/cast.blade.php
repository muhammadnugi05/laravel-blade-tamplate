@extends('layouts.master')
@section('header')
<h1>Cast Tabel</h1>
@endsection
@section('content')
<div class="container">
  <a href="/cast/create" type="button" class="mb-3 btn btn-success">Tambahkan</a> 
  <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($cast as $key => $value )
              <tr>
                <th scope="row">{{$key +1}}</th>
                <td>{{$value -> nama}}</td>
                <td>{{$value -> umur}}</td>
                <td>{{$value -> bio}}</td>
                <td class="d-flex">
                  <a href="/cast/{{$value->id}}" type="button" class="btn btn-primary">Show</a> 
                  <a href="/cast/{{$value->id}}/edit" type="button" class="ml-3 btn btn-success">Edit</a> 
                  <form class="ml-3"action="/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger" value="Delete">
                  </form>
                </td>
              </tr>
              @empty
              <tr>
                <td>No data</td>
              </tr>
              @endforelse
            </tbody>
          </table>
    </div>
@endsection